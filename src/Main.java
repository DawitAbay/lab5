import items.Bucket.WingSize;
import items.ChickenWings;
import items.ChickenWings.FreeItem;
import items.ChickenWings.WingSauce;
import items.PepperoniePizza;
import items.PepperoniePizza.PepperoniExtraToppings;
import items.Pizza;
import items.Pizza.PizzaSize;

public class Main {
	public static void main (String []args) {
		PizzaSize pSize = PizzaSize.XLARGE;
		Pizza myPizza = new PepperoniePizza(pSize);
		
		myPizza.addExtraToppings(PepperoniExtraToppings.CHERRY_TOMATO.toString());
		myPizza.addExtraToppings(PepperoniExtraToppings.PARMESAN_CHEESE.toString());
		myPizza.addExtraToppings(PepperoniExtraToppings.CHICKRN.toString());
		myPizza.PrintReceipt();
		
		
		WingSize WSize = WingSize.XLARGE;
		ChickenWings mychicken = new ChickenWings(WSize);
		mychicken.addExtraToppings(WingSauce.Sriracha.toString());
		mychicken.addExtraToppings(WingSauce.Teriyaki.toString());
		mychicken.addExtraToppings(FreeItem.onionRings.toString());
		mychicken.printReceipt();
		
	}
}
