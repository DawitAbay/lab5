package items;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import items.Pizza.PizzaSize;

public abstract class Bucket {
	
	/*
	 * 
	 * FIELDS: (MUST be 'private')
	 * 		1. Number of Pieces
	 * 		2. Price (per piece)
	 * 		3. Free side (if at all)
	 * 		
	 */
	
	
	public enum WingSize {
		MEDIUM, LARGE, XLARGE
	}
	
	private WingSize WSize;
	private double basePrice;
	private double setFreeItembasePrice;
	private List<String>wToppings;
	private List<String>freeItem;	
	
	
	
	/*
	 * 
	 * CONSTRUCTORS: (MUST BE 'protected')
	 * 		1. Bucket()
	 * 		2. Bucket(int n)
	 * 		
	 */
	public Bucket() {
		this.WSize = WingSize.MEDIUM;
		this.basePrice = 0.0;
		this.wToppings = new LinkedList<String>();
		this.freeItem = new LinkedList<String>();
	}
	protected Bucket (WingSize WSize) {
		this.WSize = WSize;
		this.basePrice = 0.0;
		this.wToppings = new LinkedList<String>();
		this.freeItem = new LinkedList<String>();
		
	}
	
	/*
	 * 
	 * METHODS:
	 * 		1. Print the Receipt
	 * 		2. calculate the price (depends on the number of pieces)
	 * 		3. Setters for all fields. (setter for price MUST BE 'protected'.)
	 * 		4. Getters for all fields. 
	 */
	
	
	//Size
	public WingSize getWingSize() {
		return WSize;
	}

	public void setPizzaSize(WingSize WSize) {
		this.WSize = WSize;
	}
	//Toppings
	public List<String> getToppings() {
		return wToppings;
	}

	protected void setToppings(List<String> toppings) {
		this.wToppings.addAll(toppings);
	}
	
	//price
	public double getBasePrice() {
		return basePrice;
	}
	
	protected void setBasePrice(double newprice){
		this.basePrice = newprice;
	}
	
	
	
	
	//FreeItem
	public List<String> getFreeItem() {
		return this.freeItem;
	}

	protected void setFreeItem(List<String> freeItem) {
		this.freeItem.addAll(freeItem);
	}
	
	protected void setFreeItemBasePrice(double price){
		this.basePrice = price;
	}
	
	
	
	//Receipt
	public void printReceipt(){

		System.out.println("TYPE:\t\t" + this.getClass().getSimpleName());
		System.out.println("SIZE:\t\t" + this.getWingSize().toString());
		System.out.println("PRICE:\t\t"  + this.getBasePrice());
		System.out.println("WingSauce:\t" + Arrays.toString(this.wToppings.toArray()));
		System.out.println("FreeItem:\t" + Arrays.toString(this.freeItem.toArray()));
		System.out.println("()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()()");
		

	}
	public void addExtraToppings(String extraTopping) {
		// TODO Auto-generated method stub
		this.wToppings.add(extraTopping);
		double currentPrice = this.getBasePrice();
		currentPrice += 1.75;
		this.setBasePrice(currentPrice);
		
	}
//	public void addFreeItems(String onionRings) {
//		this.freeItem.add(onionRings);
//	}
}