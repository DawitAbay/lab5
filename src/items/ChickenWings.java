package items;


import java.util.LinkedList;
import java.util.List;
public class ChickenWings extends Bucket{

	 
	/*
	 *  public enum WingSauce {Sriracha, Teriyaki, ...}
	 * 
	 * 	Fields: (in addition to the parent class)
	 * 		1. WING_BASE_PRICE
	 * 		2. LIST_OF_FREE_SIDES (This is similar to the list of extra topping in 'PepperoniPizza' example.)
	 * 		2. Sauce (Each sauce MUST add $0.5 to the final price.
	 * 		
	 */
	public enum WingSauce {
		Sriracha, Teriyaki, HotSauce
		
	}
	public enum FreeItem{
		onionRings;
	}
	
	final static double ChickenWings_Base_price = 05.99;
	final static List<String> CHICKENWINGS_TOPPINGS = new LinkedList<String>();
	static{
		
		}
	final static List<String> FREEITEM_SIDE = new LinkedList<String>();
	static{
		FREEITEM_SIDE.add("onion Rings");
		FREEITEM_SIDE.add("Water");
		FREEITEM_SIDE.add("bread");
	}
	/*
	 * 
	 * CONSTRUCTORS: (MUST call its parent's constructor)
	 * 		1. ChickenWings()
	 * 		2. ChickenWings(int n)
	 * 		3. ChickenWings(int n, Sauce s)
	 * 		
	 */
	public ChickenWings(){
		super();
		setBasePrice(ChickenWings_Base_price);
		setToppings(CHICKENWINGS_TOPPINGS);
		setFreeItem(FREEITEM_SIDE);
	}
	public ChickenWings(WingSize WSize){
		super();
		setBasePrice(ChickenWings_Base_price);
		setToppings(CHICKENWINGS_TOPPINGS);
		setFreeItem(FREEITEM_SIDE);
		
		
	}
	
	
	/*
	 * 
	 * METHODS:
	 * 		1. Print the Receipt (if needed, it MUST override the parent's method.)
	 * 		2. calculate the price (depends on the number of pieces and the sauce)
	 * 		3. Setters for any extra field. (Only if necessary)
	 * 		4. Getters for any extra field. (Only if necessary)
	 */
	@Override
	public void addExtraToppings(String extraTopping){
		super.addExtraToppings(extraTopping);
	}
	
}
