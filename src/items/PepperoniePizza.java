package items;

import java.util.LinkedList;
import java.util.List;

public class PepperoniePizza extends Pizza{
	
	
	
	public enum PepperoniExtraToppings {
		CHERRY_TOMATO , PARMESAN_CHEESE ,CHICKRN
	}
	final static double PEPPERONIE_BASE_PRICE = 10.99;
	final static List<String>PEPPERONIE_TOPPINGS = new LinkedList<String>();
	
	
	public PepperoniePizza() {
		super();
		setBasePrice(PEPPERONIE_BASE_PRICE);
		setTopping(PEPPERONIE_TOPPINGS);
	}
	public PepperoniePizza(PizzaSize pSize) {
		super(pSize);
		setBasePrice(PEPPERONIE_BASE_PRICE);
		setTopping(PEPPERONIE_TOPPINGS);
	}
	@Override
	public void addExtraToppings(String extraTopping){
		super.addExtraToppings(extraTopping);
	}
	

	
	
	
}
