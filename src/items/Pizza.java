package items;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class Pizza {
	
	public enum PizzaSize{
		MEDIUM ,LARGE ,XLARGE;
	}

	private PizzaSize pSize;
	private double basePrice;
	private List<String>toppings;
	
	protected  Pizza() {
		this.pSize=PizzaSize.MEDIUM;
		this.basePrice = 0.0;
		this.toppings = new LinkedList<String>();
	}
	protected Pizza(PizzaSize pSize) {
		this.pSize = pSize;
		this.basePrice = 0.0;
		this.toppings = new LinkedList<String>();
	}
	
	public void PrintReceipt() {
		System.out.println("TYPE:\t" + this.getClass().getSimpleName());
		System.out.println("SIZE:\t"+this.pSize.toString());
		System.out.println("PRICE:\t"+this.basePrice);
		System.out.println("TOPPING:\t"+ Arrays.toString(this.toppings.toArray()));
		System.out.println("()()()()()()()()()()()()()()()()()()()()()()()()()()()()()");
	}
	public void addExtraToppings(String extraToppings){
		this.toppings.add(extraToppings);
		double currentPrice = this.getBasePrice();
		currentPrice += 2.0;
		this.setBasePrice(currentPrice);
	}
	
	
	public PizzaSize getPizzaSizze() {
		return this.pSize;
	}
	public void setPizzaSize(PizzaSize pSize) {
		this.pSize = pSize;
	}
	public List<String> getToppings() {
		return this.toppings;
	}
	protected void setTopping(List<String>toppings) {
		this.toppings.addAll(toppings);
	}
	public double getBasePrice() {
		return this.basePrice;
	}
	protected void setBasePrice(double newPrice) {
		this.basePrice = newPrice;
	}
	
}
